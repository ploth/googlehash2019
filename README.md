# GoogleHash2019

## Team Strategy

1. Bring your SICK ID card and spin it around.
1. One person starts to implement a new parser. The rest is thinking about the
   problem.
1. Synchronize it each after having a working parser.
1. Implement very basic and naive solution.
1. Implement a score calculator so you know how good you are.
1. You may want to extend the basic solution by randomization.

## Code

* `problems`: input folder
* `solutions`: output folder
* The run.py is the entry point and it is basically done. It reads the arg as strategy and iterates
through all input files, applies the strategy to them and calculates the score.
* Strategies are just different functions inside the `strategies.py`

## Remarks

* Implement different strategies and improve iteratively.
* It is good to have a naive but working implementation as soon as possible.
    * Multiple people can work on different strategies at the same time.
* In the last years the best solutions were always random based.
    * The best part is, run it multiple times and get better scores (probably).
