from typing import Set, List


class Image(object):
    def __init__(self, id: int, orientation: str, amount_tags: int, tags: Set[str]):
        self.id = id
        self.orientation: str = orientation
        self.amount_tags: int = amount_tags
        self.tags: Set[str] = tags

    def __repr__(self):
        return "Image {}: \n \
Orientation: {} \n \
Amount tags: {} \n \
Tags: {} \n".format(self.id, self.orientation, self.amount_tags, self.tags)


class ImageCollection(object):
    def __init__(self, images: List[Image], amount_different_tags: int, mean_tags_per_pic: float):
        self.images = images
        self.amount_images = len(images)
        self.amount_different_tags = amount_different_tags
        self.mean_tags_per_pic = mean_tags_per_pic

    def __repr__(self):
        repr_str = "Collection: \n"
        repr_str += " - Amount images: {}\n".format(self.amount_images)
        repr_str += " - Amount different tags: {}\n".format(self.amount_different_tags)
        repr_str += " - Mean tags per pic: {}\n".format(self.mean_tags_per_pic)
        return repr_str


def get_lines_from_file(path) -> List[str]:
    with open(path, 'r') as file:
        content = file.read()

    return [line for line in content.split("\n") if len(line) > 0]


def interpret_lines(lines) -> ImageCollection:
    # get attributes from first line
    amount_images = int(lines[0])
    images = list()
    all_tags_set = set()
    running_amount_tags = 0
    for id, line in enumerate(lines[1:]):
        # Split the line
        split_line = line.split()
        orientation = split_line[0]
        amount_tags = split_line[1]
        tags_set = set(split_line[2:])
        all_tags_set.update(tags_set)
        running_amount_tags += len(split_line[2:])
        images.append(Image(id, orientation, amount_tags, tags_set))

    return ImageCollection(images, len(all_tags_set), float(running_amount_tags) / len(images))


def load_image_collection(path):
    lines = get_lines_from_file(path)
    image_collection = interpret_lines(lines)
    image_collection.path = path
    return image_collection


def save_solution(output_path: str, slideshow):
    amount_slides = len(slideshow.slides)
    solution_str = ""
    solution_str += "{}\n".format(amount_slides)
    for slide in slideshow.slides:
        if slide.image2 is None:
            solution_str += "{}\n".format(slide.image1.id)
        else:
            solution_str += "{} {}\n".format(slide.image1.id, slide.image2.id)

    with open(output_path, 'w') as file:
        file.write(solution_str)
    print("Stored solution in: {}".format(output_path))
