#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
from typing import List

import strategies

from parser import ImageCollection, Image
from parser import load_image_collection, save_solution
from simulate import Slideshow


def main():
    # Argument parser
    arg_parser = argparse.ArgumentParser(
        description="Runs the hashcode solution",
        formatter_class=argparse.RawTextHelpFormatter)

    # Handle arguments
    arg_parser.add_argument("strategy_name", help="Strategy name")
    args = arg_parser.parse_args()
    strategy_name = args.strategy_name

    total_score = 0

    # Iterate through input files of the problems folder
    for filename in sorted(os.listdir("./problems")):
        # Load input file
        image_collection = load_image_collection("./problems/{}".format(filename))
        print(image_collection)

        # Apply strategy to problem
        print("Applying Strategy")
        slideshow: Slideshow = strategies.__dict__[strategy_name](image_collection)

        # Calculate score
        print("Calculating score:")
        score = slideshow.calculate_score()
        print("Score: {}".format(score))

        # Save solution
        save_solution("./solutions/{}_{}.out".format(filename, strategy_name), slideshow)

        total_score = total_score + score
        print("")
        print("")

    print("Total score: {}".format(total_score))


if __name__ == '__main__':
    main()
