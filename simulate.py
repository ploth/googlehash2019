from typing import List, Optional, Set

from parser import ImageCollection, Image


class Slide(object):
    def __init__(self, image1: Image, image2: Optional[Image] = None):
        self.image1: Image = image1
        self.image2: Image = image2

        if image2 is None and image1.orientation == "V":
            raise IOError("Found slide with only one vertical image! Image: {}".format(image1.id))
        if image2 is not None:
            if not image1.orientation == image2.orientation == "V":
                print(image1)
                print(image2)
            assert image1.orientation == image2.orientation == "V", "Mixed orientation images on same slide!"

    def get_tags(self) -> Set[str]:
        if self.image2 is None:
            return self.image1.tags
        else:
            return self.image1.tags.union(self.image2.tags)


class Slideshow(object):
    def __init__(self, slides: List[Slide]):
        self.slides = slides

    def calculate_score(self) -> int:
        # Go through slide pairs
        accumulated_score = 0
        for i in range(len(self.slides) - 1):
            slide1: Slide = self.slides[i]
            slide2: Slide = self.slides[i + 1]
            accumulated_score += score_slide_pair(slide1, slide2)

        return accumulated_score


def score_slide_pair(slide1: Slide, slide2: Slide):
    # Calc criteria
    common_tags = slide1.get_tags().intersection(slide2.get_tags())
    amount_common_tags = len(common_tags)

    different_tags1 = slide1.get_tags().difference(slide2.get_tags())
    amount_different_tags1 = len(different_tags1)

    different_tags2 = slide2.get_tags().difference(slide1.get_tags())
    amount_different_tags2 = len(different_tags2)

    # Calc score
    return min(amount_common_tags, amount_different_tags1, amount_different_tags2)
