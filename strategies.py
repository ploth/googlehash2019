from typing import List
import pickle
import os
from tqdm import tqdm

from parser import ImageCollection, Image
from simulate import Slide, Slideshow, score_slide_pair


def stupidly_concatenate_images(image_collection: ImageCollection) -> Slideshow:
    slides: List[Slide] = list()
    for i in range(0, len(image_collection.images), 2):
        image1: Image = image_collection.images[i]
        image2: Image = image_collection.images[i + 1]

        if image1.orientation == image2.orientation == 'V':
            slides.append(Slide(image1, image2))
        if image1.orientation == 'H':
            slides.append(Slide(image1))
        if image2.orientation == 'H':
            slides.append(Slide(image2))

        # Mixed images will be dropped

    return Slideshow(slides)


def sort_by_amount_tags_then_HV(image_collection: ImageCollection) -> Slideshow:
    slides: List[Slide] = list()

    # Sort by the amount of tags
    sorted_by_amount_tags = sorted(image_collection.images, key=lambda k: len(k.tags), reverse=True)

    # Sort V next to each other
    skip_indicies = list()
    for i, image in enumerate(tqdm(sorted_by_amount_tags)):
        if i in skip_indicies:
            continue

        if image.orientation == "H":
            slides.append(Slide(image))
        elif image.orientation == "V":
            # Search next V image
            for j, image2 in enumerate(sorted_by_amount_tags[i + 1:]):
                k = j + i + 1
                if image2.orientation == "V":
                    slides.append(Slide(image, image2))
                    skip_indicies.append(k)
                    break

    return Slideshow(slides)


def sort_by_amount_tags_then_HV2(image_collection: ImageCollection) -> Slideshow:
    slides: List[Slide] = list()

    # Sort by the amount of tags
    sorted_by_amount_tags = sorted(image_collection.images, key=lambda k: len(k.tags), reverse=True)

    # Sort V next to each other
    skip_indicies = list()
    from tqdm import tqdm
    for i, image in enumerate(tqdm(sorted_by_amount_tags)):
        if i in skip_indicies:
            continue

        if image.orientation == "H":
            slides.append(Slide(image))
        elif image.orientation == "V":
            # Search next V image
            for j, image2 in enumerate(sorted_by_amount_tags[i + 1:]):
                k = j + i + 1
                if image2.orientation == "V":
                    slides.append(Slide(image, image2))
                    skip_indicies.append(k)
                    break

    slides = sorted(slides, key=lambda k: len(k.get_tags()), reverse=True)
    return Slideshow(slides)


def sort_by_amount_tags_then_HV3(image_collection: ImageCollection) -> Slideshow:
    slides: List[Slide] = list()

    # Sort by the amount of tags
    sorted_by_amount_tags = sorted(image_collection.images, key=lambda k: len(k.tags), reverse=True)

    # Sort V next to each other
    skip_indicies = list()
    from tqdm import tqdm
    for i, image in enumerate(tqdm(sorted_by_amount_tags)):
        if i in skip_indicies:
            continue

        if image.orientation == "H":
            slides.append(Slide(image))
        elif image.orientation == "V":
            # Search next V image
            for j, image2 in enumerate(sorted_by_amount_tags[i + 1:]):
                k = j + i + 1
                if image2.orientation == "V":
                    slides.append(Slide(image, image2))
                    skip_indicies.append(k)
                    break

    slides = sorted(slides, key=lambda k: len(k.get_tags()), reverse=True)

    # Special handling
    if image_collection.amount_different_tags == 840000:
        print('Special handling for Dataset b')
        new_slides = list()
        exclude_id = list()
        for i, slide1 in enumerate(slides):
            if i in exclude_id:
                continue
            for j, slide2 in enumerate(slides):
                if j in exclude_id:
                    continue
                if score_slide_pair(slide1, slide2):
                    new_slides.append(slide1)
                    new_slides.append(slide2)
                    exclude_id.append(i)
                    exclude_id.append(j)

        slides = new_slides
    return Slideshow(slides)


def sort_by_amount_tags_then_HV_threshold(image_collection: ImageCollection) -> Slideshow:
    # Load pickle if exists
    pickle_file = "slideshow_{}.pickle".format(
        os.path.basename(image_collection.path).replace(".txt", ""),
    )
    if not os.path.isfile(pickle_file):
        slideshow = sort_by_amount_tags_then_HV2(image_collection)
        with open(pickle_file, 'wb') as file:
            pickle.dump(slideshow, file)
    else:
        with open(pickle_file, 'rb') as file:
            slideshow = pickle.load(file)

    slides = slideshow.slides

    threshold = int(image_collection.mean_tags_per_pic / 2)
    final_slides = list()
    final_slides.append(slides[0])

    def run_separation(slide_list, final_slides_list, current_threshold):
        slides_for_later = list()
        for i in range(len(slide_list) - 1):
            slide1: Slide = slide_list[i]
            slide2: Slide = slide_list[i + 1]

            score = score_slide_pair(slide1, slide2)

            if score < current_threshold:
                slides_for_later.append(slide2)
            else:
                final_slides_list.append(slide2)
        return slides_for_later

    pickle_file = "thresholded_{}.pickle".format(
        os.path.basename(image_collection.path).replace(".txt", ""),
    )
    if not os.path.isfile(pickle_file):
        for i in tqdm(reversed(range(threshold)), desc="Thresholding"):
            slides = run_separation(slides, final_slides, i)
        with open(pickle_file, 'wb') as file:
            pickle.dump(final_slides, file)
    else:
        with open(pickle_file, 'rb') as file:
            final_slides = pickle.load(file)

    return Slideshow(final_slides)
